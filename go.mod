module gitlab.com/sergiolucena1/golang.git

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/gofiber/fiber/v2 v2.21.0
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20211107104306-e0b2ad06fe42 // indirect
	gorm.io/driver/postgres v1.2.1
	gorm.io/gorm v1.22.2
)
