package main

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/sergiolucena1/golang.git/product"
)





func main() {
	product.InitialMigration()
	app := fiber.New()

	product.SetupRoutes(app)

	app.Listen(":3000")
}
