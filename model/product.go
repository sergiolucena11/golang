package model

import (
	"gorm.io/gorm"
)

type Product struct {
	gorm.Model
	Name        string         `json:"name"`
	Description string         `json:"description"`
	MediumPrice float64        `json:"medium_price"`

}
