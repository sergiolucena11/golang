package product

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/sergiolucena1/golang.git/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

var DB *gorm.DB
var err error

const DNS = "host=localhost user=postgres password=postgres dbname=products port=5432 sslmode=disable "

func InitialMigration() {
	DB, err = gorm.Open(postgres.Open(DNS), &gorm.Config{})
	if err != nil {
		log.Fatal(err)

	}
	DB.AutoMigrate(&model.Product{})
}



func GetProducts(c *fiber.Ctx) error {
	var products []model.Product
	DB.Find(&products)
	return c.JSON(&products)
}

func GetProduct(c *fiber.Ctx) error {
	id := c.Params("id")
	var product model.Product
	DB.Find(&product, id)
	return c.JSON(&product)

}

func SaveProduct(c *fiber.Ctx) error {
	product := new(model.Product)
	if err := c.BodyParser(product); err != nil {
		return c.Status(500).SendString(err.Error())
	}
	DB.Create(&product)

	return c.JSON(product)
}

func DeleteProduct(c *fiber.Ctx) error {
	id := c.Params("id")
	var product model.Product
	DB.First(&product, id)
	if product.Name == "" {
		return c.Status(404).SendString("Product not found")
	}
	DB.Delete(&product)
	return c.SendString("Product is deleted!")
}

func UpdateProduct(c *fiber.Ctx) error {
	id := c.Params("id")
	product := new(model.Product)
	DB.First(product, id)

	if product.Name == "" {
		return c.Status(500).SendString("Product not available")
	}
	if err := c.BodyParser(&product); err != nil {
		return c.Status(500).SendString(err.Error())
	}
	DB.Save(&product)
	return c.JSON(&product)

}
