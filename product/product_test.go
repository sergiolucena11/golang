package product

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sergiolucena1/golang.git/model"
	"io/ioutil"
	"net/http/httptest"
	"testing"
)

type Test []struct {
	method       string
	description  string
	route        string
	postdata     model.Product
	expectedCode int
}

var payloadTest = model.Product{

	Name:        "iPhone",
	Description: "Muito bom",
	MediumPrice: 100,
}

func TestGetProducts(t *testing.T) {
	tests := Test{
		{
			method:       "GET",
			description:  "Get HTTP status 200",
			route:        "/api/vi/products",
			expectedCode: 200,
			postdata:     payloadTest,
		},

		{
			description:  "get HTTP status 404, when route is not exists",
			route:        "/not-found",
			expectedCode: 404,
		},
	}
	InitialMigration()
	app := fiber.New()
	SetupRoutes(app)

	for _, test := range tests {

		req := httptest.NewRequest(test.method, test.route, nil)

		resp, err := app.Test(req, -1)
		if err != nil {
			t.Error(err)
		}

		assert.Equalf(
			t,
			test.expectedCode,
			resp.StatusCode,
			test.description)

	}
}

func TestGetProduct(t *testing.T) {

	InitialMigration()
	app := fiber.New()
	SetupRoutes(app)

	url := fmt.Sprintf("/api/v1/product/%d", payloadTest.ID)
	fmt.Println(url)
	req := httptest.NewRequest("GET", url, nil)
	resp, _ := app.Test(req, -1)
	body, _ := ioutil.ReadAll(resp.Body)

	var prod model.Product
	json.Unmarshal([]byte(body), &prod)
	fmt.Println(string(body))
	assert.Equalf(
		t,
		200,
		resp.StatusCode,
		"post TestGetProduct to get product by id")

	assert.Equal(
		t,
		//`[{"ID":2,"CreatedAt":"2021-11-16T12:23:30.244719-03:00","UpdatedAt":"2021-11-16T12:23:30.244719-03:00","DeletedAt":null,"name":"teste23","description":"teste","medium_price":20},{"ID":3,"CreatedAt":"2021-11-16T14:30:00.843945-03:00","UpdatedAt":"2021-11-16T14:30:00.843945-03:00","DeletedAt":null,"name":"teste23","description":"teste","medium_price":20},{"ID":4,"CreatedAt":"2021-11-17T10:23:00.461748-03:00","UpdatedAt":"2021-11-17T10:23:00.461748-03:00","DeletedAt":null,"name":"teste23","description":"teste","medium_price":20}]`,
		payloadTest.ID,
		prod.ID,
	)
}

func TestSaveProduct(t *testing.T) {
	InitialMigration()
	app := fiber.New()
	SetupRoutes(app)

	payloadBytes, _ := json.Marshal(payloadTest)
	bodyReq := bytes.NewReader(payloadBytes)

	req := httptest.NewRequest("POST", "/api/v1/product", bodyReq)
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	resp, _ := app.Test(req, -1)
	body, _ := ioutil.ReadAll(resp.Body)

	var prod model.Product

	json.Unmarshal(body, &prod)
	payloadTest.ID = prod.ID
	assert.Equalf(
		t,
		200,
		resp.StatusCode,
		"post Test_newProduct to create new productd")

	assert.Equal(
		t,
		payloadTest.ID,
		prod.ID,
		string(body),
	)
}

func TestUpdateProduct(t *testing.T) {
	InitialMigration()
	app := fiber.New()
	SetupRoutes(app)

	payloadTest.ID = 4
	payloadBytes, _ := json.Marshal(payloadTest)
	bodyReq := bytes.NewReader(payloadBytes)
	url := fmt.Sprintf("/api/v1/product/%d", payloadTest.ID)
	req := httptest.NewRequest("PUT", url, bodyReq)
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	resp, _ := app.Test(req, -1)
	body, _ := ioutil.ReadAll(resp.Body)

	var prod model.Product
	json.Unmarshal([]byte(body), &prod)

	assert.Equalf(
		t,
		200,
		resp.StatusCode,
		"patch Test_patchProduct to update a product")

	assert.Equal(
		t,
		uint(4),
		prod.ID,
		string(body),
	)

}

func TestDeleteProduct(t *testing.T) {

	InitialMigration()
	app := fiber.New()
	SetupRoutes(app)

	//save a product for test delete
	payloadBytes, _ := json.Marshal(payloadTest)
	bodyReq := bytes.NewReader(payloadBytes)
	req := httptest.NewRequest("POST", "/api/v1/product", bodyReq)
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	resp, _ := app.Test(req, -1)
	body, _ := ioutil.ReadAll(resp.Body)

	var prod model.Product

	json.Unmarshal(body, &prod)
	payloadTest.ID = prod.ID

	tests := Test{
		{
			method:       "DELETE",
			description:  "Delete HTTP status 200",
			route:        fmt.Sprintf("/api/v1/product/%d",prod.ID),
			expectedCode: 200,
		},
		{
			method:       "DELETE",
			description:  "Product not found",
			route:        fmt.Sprintf("/api/v1/product/%d",prod.ID),
			expectedCode: 404,
		},
	}



	for _, test := range tests {

		req := httptest.NewRequest(test.method, test.route, nil)

		resp, err := app.Test(req, -1)
		if err != nil {
			t.Error(err)
		}

		fmt.Println(resp.Status)

		assert.Equalf(
			t,
			test.expectedCode,
			resp.StatusCode,
			test.description)

	}
}
