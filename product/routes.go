package product

import (
	"github.com/gofiber/fiber/v2"
)

func SetupRoutes(app *fiber.App){
	app.Get("/api/vi/products", GetProducts)
	app.Get("/api/v1/product/:id", GetProduct)
	app.Post("/api/v1/product", SaveProduct)
	app.Delete("/api/v1/product/:id", DeleteProduct)
	app.Put("/api/v1/product/:id", UpdateProduct)
}
